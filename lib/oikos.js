module.exports = (function() {
  var masala = {};
  // So we can 'pass in' an operand as a 'function'
  // PS. Javascript gods pls.
  var op = (function(op) {
    switch (op) {
      case '+':
        return (function(a, b) {
          return a + b;
        });
      case '-':
        return (function(a, b) {
          return a - b;
        });
      case '*':
        return (function(a, b) {
          return a * b;
        });
      case '/':
        return (function(a, b) {
          return a / b;
        });
      case '%':
        return (function(a, b) {
          return a % b;
        });
    }
  });
  masala.op = op;
  // Verify if the object passed into function is an array.
  masala.isArray = (function(arry) {
    return (arry) ? arry.constructor === Array : false;
  });

  // Converts arguments into an array, optional slicing
  masala.toArray = (function(array, init) {
    var newArray = [];
    init = init || 0; // default to 0;
    if (!array || init >= array.length) return newArray; // If they try something silly.
    if (this.isArray(array[init])) return array[init]; // if they pass in an actual array.

    return (function recur(array, cur_index) {
      if (array[cur_index])
        newArray.push(array[cur_index]);
      return (cur_index < array.length - 1) ? recur(array, cur_index + 1) : newArray;
    })(array, init);

  });

  // Get the first item in an array or an argument list
  masala.first = (function() {
    return arguments.length ? this.toArray(arguments)[0] : [];
  });

  // Get the 1st index and onward of the arguments
  masala.rest = (function() {
    return arguments ? this.toArray(arguments, 1) : [];
  });
  // Get the last item in an array or argument list
  masala.last = (function() {
    if (!arguments.length) return [];
    var args = this.toArray(arguments);
    return (function recur(args) {
      return args.length - 1 ? recur(masala.rest.apply(masala, args)) : masala.first(args);
    })(args);
  });
  // Recursive because why not?
  masala.forEach = (function(func) {
    var args = this.rest.apply(this, arguments);

    return (function recur(args) {
      if (masala.first(args))
        func(masala.first(args));
      return args.length ? recur(masala.rest.apply(masala, args)) : null;
    })(args);

  });
  masala.negate = (function(func) {
    return (function() {
      return !func.apply(null, arguments);
    });
  });

  // Verify that every element in satisfies a certain requirement (eg. prime, even, etc..)
  masala.every = (function(fn) {
    var args = this.rest.apply(this, arguments),
      every = true;
    this.forEach(function(element) {
      if (!(fn.call(null, element))) every = false;
    }, args);
    return every;
  });
  /* Curry a function, meaning that you pass in one parameter and then the others at a later time
    This is useful for making a function that builds other functions
  */
  masala.curry = (function(fn) {
    var args = this.rest.apply(this, arguments);
    return (function() {
      return fn.apply(this, args.concat(masala.toArray(arguments)));
    });
  });
  // Extends the concept of currying to include multiple arity (more than one parameter)
  masala.partial = (function(fn) {
    var arity = fn.length,
      args = this.rest.apply(this, arguments);
    lamb = (function() {
      var lambda = args;
      if (arguments.length)
        lambda = lambda.concat(masala.toArray(arguments));
      return lambda.length >= arity ? fn.apply(this, lambda) : masala.partial.apply(this, [fn].concat(lambda));
    });
    return args.length >= arity ? lamb() : lamb;
  });

  masala.sequence = (function() {
    var fns = this.toArray(arguments);
    return fns.reduce(function(f, g) {
      return (function() {
        return g(f.apply(this, arguments));
      });
    });
  });
  // Recursive map function
  masala.map = (function(fn) {
    var args = this.rest.apply(this, arguments),
      newArray = [];
    return (function recur(args) {
      if (masala.first(args))
        newArray.push(fn.call(this, masala.first(args)));
      return (!args.length) ? newArray : recur(masala.rest.apply(masala, args));
    })(args);
  });
  // This is basically fold without an explicit initial value
  masala.reduce = (function(fn) {
    var args = this.rest.apply(this, arguments), // get args
      total = this.first.apply(this, args) || 0, // Total is set to first argument
      others = this.rest.apply(this, args);

    return (function recur(total, others) {
      total = fn(total, masala.first(others));
      var rem = masala.rest.apply(masala, others);
      return rem.length ? recur(total, rem) : total;
    })(total, others);

  });
  // Execute functional composition (f(g))
  // NOTE that functions are executed from right to left, see sequence below for left to right
  masala.compose = (function() {
    var fns = this.toArray(arguments);
    return masala.reduce(function(f, g) {
      return (function() {
        return f(g.apply(this, arguments));
      });
    }, fns);
  });
  // Want to make this independent of compose for clarity's sake
  masala.sequence = (function() {
    var fns = this.toArray(arguments);
    return masala.reduce(function(f, g) {
      return (function() {
        return g(f.apply(this, arguments));
      });
    }, fns);
  });
  var cube = (function(a) {
    return masala.reduce(op('*'), a, a, a);
  });
  (function() {
    var even = (function(item) {
      return item % 2 === 0;
    });
    console.log(masala.first(1, 2, 3));
  })();
  return masala;
})();