var lambda = require('../lib/oikos');
var assert = require('assert');

describe('lambda', function() {
 describe('#isArray', function() {
   it('should return true for arrays', function() {
     assert(lambda.isArray([1, 2, 3]));
   });
   it('should return true for empty arrays', function() {
     assert(lambda.isArray([]));
   });
   it('should return false for Array-like objects', function() {
     assert(!lambda.isArray({"0": 1, length: 1}));
   });
   it('should return false for argument lists', function() {
     assert(!lambda.isArray(1, 2, 3));
   });
   it('should return false for a null parameter', function() {
     assert(!lambda.isArray(null));
   });
   it('should return false for an empty parameter', function() {
     assert(!lambda.isArray());
   });
 });
 describe('#toArray', function() {
   it('should return an array from an array', function() {
     assert.deepEqual(lambda.toArray([1, 2, 3], 0), [1, 2, 3]);
   });
   it('should return an array from an Array-like Object', function() {
     assert.deepEqual(lambda.toArray({0: 1, 1: 2, 2: 3, length: 3}), [1, 2, 3]);
   });
   it('should slice an array given an index', function() {
     assert.deepEqual(lambda.toArray([1, 2, 3], 1), [2, 3]);
   });
   it('should return the complete array given no index', function() {
     assert.deepEqual(lambda.toArray([1, 2, 3]), [1, 2, 3]);
   });
   it('should return an empty array from an argument list', function() {
     assert.deepEqual(lambda.toArray(1, 2, 3), []);
   });
   it('should return an empty array from an empty parameter', function() {
     assert.deepEqual(lambda.toArray(), []);
   });
 });
 describe('#first', function() {
   it('should return the first element in an Array', function() {
     assert.deepEqual(lambda.first([1, 2, 3]), 1);
   });
   it('should return the first element in an Array-like object', function() {
     assert.deepEqual(lambda.first.apply(lambda, {0: 1, 1: 2, 2: 3, length: 3} ), 1);
   });
   it('should return the first item in an argument list', function() {
     assert.deepEqual(lambda.first(1, 2, 3), 1);
   });
   it('should return undefined given null as an argument', function() {
     assert.deepEqual(lambda.first(null), undefined);
   });
   it('should return an empty array given no arguments', function() {
     assert.deepEqual(lambda.first(), []);
   });
 });
 describe('#rest', function() {
   it('should return all but the first element of an array', function() {
     assert.deepEqual(lambda.rest.apply(lambda, [1, 2, 3]), [2, 3]);
   });
   it('should return an array with all but the first element of an Array-like Object', function() {
     assert.deepEqual(lambda.rest.apply(lambda, {0:1, 1:2, 2:3, length: 3}), [2, 3]);
   });
   it('should return an array with all but the first element of an argument list', function() {
     assert.deepEqual(lambda.rest(1, 2, 3), [2, 3]);
   });
   it('should return an empty array given null as an argument', function() {
     assert.deepEqual(lambda.rest(null), []);
   });
   it('should return an empty array given no arguments', function() {
     assert.deepEqual(lambda.rest(), []);
   });
 });
 describe('#last', function() {
   it('should return the last element of an array', function() {
     assert.deepEqual(lambda.last([1, 2, 3]), 3);
   });
   it('should return the last element of an Array-like object', function() {
     assert.deepEqual(lambda.last.apply(lambda, {0:1, 1:2, 2:3, length: 3}), 3);
   });
   it('should return the last element of an argument list', function() {
     assert.deepEqual(lambda.last(1, 2, 3), 3);
   });
   it('should return an empty array from empty arguments', function() {
     assert.deepEqual(lambda.last(), []);
   });
   it('should return an empty array from a null argument', function() {
     assert.deepEqual(lambda.last.apply(lambda, null), []);
   });
 });
 describe('#negate', function() {
   var even = (function(x) {
     return x % 2 === 0;
   });
   var prime = (function(x) {
     var i = 0;
     while(i < Math.sqrt(x)) {
       if(x % i === 0) return false;
       i++;
     }
     return true;
   });   
   it('should test for odds', function() {
     assert(lambda.negate(even)(3)); // 3 is NOT even.
   });
   it('should test for nonprimes', function() {
     assert(lambda.negate(prime)(4)); // 4 is NOT prime.
   });
 });
 describe('#every', function() {
   var even = (function(x) {
     return x % 2 === 0;
   });
   var prime = (function(x) {
     var i = 0;
     while(i < Math.sqrt(x)) {
       if(x % i === 0) return false;
       i++;
     }
     return true;
   }); 
   it('should return true when all elements satisfy the condition', function() {
     assert(lambda.every(even, [2, 4, 6]));
   });
   it('should return true when all elements satisfy the condition', function() {
     assert(lambda.every(prime), [1, 2, 3]);
   });
   it('should return false when one or more of the elements fails to satisfy the condition', function() {
     assert(!lambda.every(even, [2, 3, 4]));
   });
   it('should return false when one or more of the element fails to satisfy the condition', function() {
     assert(!lambda.every(prime, [2, 3, 4]));
   });
 });
 (function() {
   console.log(lambda.forEach);
   lambda.forEach(function(element) {
     console.log('noice', element);
   }, 1, 2, 3);
 })();
});
