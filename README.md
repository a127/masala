# Oikos #
## A small functional library for fun ##
Offers a lightweight utility belt with some common functions associated with the functional programming paradigm. Most functions are implemented using recursion and not iteration (as iteration is ugly in my humble opinion). Feel free to email me with suggestions (or to violently rage against my abysmal coding).

Note that when passing in Array-like Objects to some functions (first, rest, etc..) you need to follow the oikos.apply(oikos {object); This was by design (laziness) but I'll eventually restructure the module to be more consistent.

## Installation ##

```
#!javascript

npm install oikos
```
## Get Started ##

```
#!javascript

var oikos = require('oikos');
```

## isArray(array) ##
Fairly straightforward. Checks the constructor of the parameter.

### Examples ###
```
#!javascript
oikos.isArray([1, 2, 3])                 // true
oikos.isArray({0:1, 1:2, 2:3, length:3}) // false
oikos.isArray([])                        // true
```

## toArray(array, (optional) index) ##
Almost every function leverages toArray, which not surprisingly, converts arguments into an array.

### Examples ###
```
#!javascript

oikos.toArray([1, 2, 3]);                    // [1, 2, 3]
oikos.toArray({0:1, 1:2, 2:3, length: 3});   // [1, 2, 3]

You can also create a new array 'sliced' from a given index. 

oikos.toArray([1, 2, 3], 1)                  // [2, 3]
oikos.toArray({0:1, 1:2, 2:3, length: 3}, 1) // [2, 3]

NOTE: Passing in lone arguments will return an empty array
(e.g) oikos.toArray(1, 2, 3);
I am considering this implementation in the future.

```

## first() ##
Just like it sounds. Return the first element of an Array, Array-like object, or arguments.

### Examples ###

```
#!javascript

oikos.first([1, 2, 3])                              // 1
oikos.first.apply(oikos, {0:1, 1:2, 2:3, length:3}) // 1
oikos.first(1, 2, 3)                                // 1 
```

## rest() ##
Returns an Array with the rest of the initial array, similar to Array.slice(1) except mine is recursive. Tread lightly.

### Examples ###

```
#!javascript

oikos.rest([1, 2, 3])                               // [2, 3]
oikos.rest.apply(oikos, {0:1, 1:2, 2:3, length: 3}) // [2, 3]
oikos.rest(1, 2, 3)                                 // [2, 3]
```

## last() ##
Returns the last element in an Array, Array-like Object, or arguments. Implemented with recursion for elegance.

### Examples ###

```
#!javascript

oikos.last([1, 2, 3])                              // 3
oikos.last.apply(oikos, {0:1, 1:2, 2:3, length:3}) // 3
oikos.last(1, 2, 3)                                // 3
```

## forEach(fn) ##
Recursive implementation of the generic Array method of the same name. Slightly different syntax, adopted to accommodate Arrays and argument lists. Does NOT work with Array-like Objects.. ..yet.

### Examples ###

```
#!javascript

var forEach = oikos.forEach;

forEach(function(element) {console.log(element}, [1, 2, 3]) // 1 2 3
forEach(function(element) {console.log(element}, 1, 2, 3)   // 1 2 3

Does not work with Array-like Objects.

```

## negate(fn) ##
A utility function that returns the opposite value of a given function. Useful for reusing a function such as isEven or isPrime.

### Examples ###

```
#!javascript

var isEven = (function(x){ return !(x % 2) })
var isOdd  = oikos.negate(isEven);

isEven(2) // true
isOdd(3)  // true

Additionally, this is useful for passing into Array.prototype.filter or oikos.filter:

[1, 2, 3, 4, 5, 6].filter(negate(isEven))          // [1, 3, 5]
oikos.filter(negate(isEven), [1, 2, 3, 4, 5, 6])   // [1, 3, 5] 

```



## every(fn) ##
## curry(fn) ##
## partial(fn) ##
## map(fn) ##
## reduce(fn) ##
## compose() ##
## sequence() ##